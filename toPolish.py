class Stack:
    def __init__(self):
        self.items = []

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def is_empty(self):
        return (self.items == [])

    def top(self):
        return self.items[len(self.items) - 1]


def toPolish(input):
    boperators = ['+', '-', '*', '/']
    joperators = ['^', '=']
    operators = ['+', '-', '*', '/', '^', '=', ')', '(']
    output = ""

    s = Stack()
    for x in input:
        if x == ' ':
            pass
        elif operators.count(x) == 0:
            output += x
        elif x == '(':
            s.push(x)
        elif x == ')':
            while s.top() != '(':
                output += s.pop()
            s.pop()
        elif boperators.count(x) == 1:
            while (not s.is_empty()) and s.top() != '(' and prec(s.top()) >= prec(x):
                output += s.pop()
            s.push(x)
        elif joperators.count(x) == 1:
            while (not s.is_empty()) and s.top() != '(' and prec(s.top()) > prec(x):
                output += s.pop()
            s.push(x)

    while not s.is_empty():
        output += s.pop()

    return output


def prec(input):
    if input == '=':
        return 1
    if input == '+' or input == '-':
        return 2
    if input == '*' or input == '/':
        return 3
    if input == '^':
        return 4


while True:
    print("postfix:" + toPolish(input("infix:")))
